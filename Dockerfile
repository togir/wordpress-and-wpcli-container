FROM docker.io/alpine:3 as downloader
WORKDIR /app
RUN apk add wget gpg gpg-agent
ADD public-key.gpg .
RUN wget -nv -O wp-cli.phar.gpg https://github.com/wp-cli/wp-cli/releases/download/v2.10.0/wp-cli-2.10.0.phar.gpg
RUN gpg --import public-key.gpg
RUN gpg --batch --decrypt --output wp-cli.phar wp-cli.phar.gpg
RUN chmod +x wp-cli.phar

From docker.io/wordpress:php8.3
RUN mkdir -p /urs/local/bin/wp
COPY --from=downloader /app/wp.cli.phar /usr/local/bin/wp/wp-cli
